DROP TABLE meal; DROP TABLE food;


CREATE TABLE food (
  id SERIAL PRIMARY KEY,
  name VARCHAR NOT NULL,
  proteins REAL NOT NULL DEFAULT 0 CHECK (proteins >= 0),
  fats REAL NOT NULL DEFAULT 0 CHECK (fats >= 0),
  carbs REAL NOT NULL DEFAULT 0 CHECK (carbs >= 0),
  calories REAL NOT NULL DEFAULT 0 CHECK (calories >= 0),
  final_weight BOOLEAN NOT NULL DEFAULT FALSE,
  deleted BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE meal (
  weight REAL CHECK (weight > 0),
  eated_at TIMESTAMP NOT NULL DEFAULT now(),
  food INTEGER REFERENCES food
);

