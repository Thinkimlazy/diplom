const express = require("express");
const { Client } = require("pg");
const bodyParser = require("body-parser");
const moment = require("moment");

const client = new Client({
  user: "postgres",
  host: "localhost",
  password: "1",
  database: "diplom"
});
const app = express();
app.use(bodyParser.json()); // for parsing application/json
var apiRouter = express.Router();

client.connect();

apiRouter.get("/food", (req, response) => {
  client.query("SELECT * from food WHERE deleted = False")
    .then(result => response.send(result.rows))
    .catch(err => {
      console.log(err)
      response.status(500).send(err)
    })
});

apiRouter.delete("/food", (req, res) => {
  const id = req.query.id;
  if (!id) {
    res.status(404).send("no id");
  }
  client
    .query("UPDATE food SET deleted = True WHERE id = $1", [id])
    .then(result => res.send(result.rows))
    .catch(err => res.status(500).send(err));
});

const createFood =
  "INSERT INTO food(name, proteins, fats, carbs, calories, final_weight) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *";
apiRouter.post("/food", (req, res) => {
  const values = getReqParams(req, [
    "name",
    "proteins",
    "fats",
    "carbs",
    "calories",
    "final_weight"
  ]);
  client
    .query(createFood, values)
    .then(result => {
      res.send(result.rows);
    })
    .catch(err => {
      res.status(500).send(err);
      console.log("error creating food", err);
    });
});

apiRouter.get("/meal", (req, response) => {
  const date = req.query.date || moment().format("YYYY-MM-DD");
  client
    .query("SELECT * from meal WHERE eated_at::date = $1", [date])
    .then(result => {
      response.send(result.rows);
    })
    .catch(err => {
      console.log(err);
      response.status(500).send(err);
    });
});

const createMeal =
  "INSERT INTO meal(food, weight, eated_at) VALUES ($1, $2, $3) RETURNING *";
apiRouter.post("/meal", (req, res) => {
  const values = getReqParams(req, ["food", "weight", "eated_at"]);
  client
    .query(createMeal, values)
    .then(result => {
      res.send(result.rows);
    })
    .catch(err => {
      res.status(500).send(err);
      console.log("error creating meal", err);
    });
});

apiRouter.delete("/meal", (req, res) => {
  const id = req.query.id;
  if (!id) {
    res.status(404).send("no id");
  }
  client
    .query("DELETE FROM meal WHERE id = $1", [id])
    .then(result => res.send(result.rows))
    .catch(err => res.status(500).send(err));
});

app.use("/api", apiRouter);

const users = [
  {
    name: "Вася",
    pass: "123"
  },
  {
    name: "Егор",
    pass: "123"
  }
];

app.listen(8000, () => {
  console.log("api runs at 8000");
});

function getReqParams(req, params) {
  return params.map(val => req.body[val]);
}
