import axios from 'axios';

export const foodIntake = {
    state: [],
    reducers: {
        add(state, payload) {
            return state.concat(payload)
        }
    }
}

export const food = {
  state: [],
  reducers: {
      load(state, payload) {
          return payload
      }
  },
  effects: dispatch => ({
    async reqFoods(payload, rootState) {
      const { data } = await axios.get('/api/food');
      dispatch.food.load(data)
    },
    async submitFood(payload, rootState) {
      const { data } = await axios.post('/api/food', payload)
      dispatch.food.reqFoods()
    }
  })
}

export const meals = {
  state: [],
  reducers: {
    load(state, payload) {
      return payload
    }
  },
  effects: dispatch => ({
    async reqMeals(date, rootState) {
      const { data } = await axios.get(`/api/meal?date=${date}`)
      dispatch.meals.load(data)
    }
  })
}
