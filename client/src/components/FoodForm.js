import React, { Component } from "react";
import { connect } from 'react-redux';
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
class FoodForm extends Component {
  state = {
    calories: 0,
    carbs: 0,
    proteins: 0,
    fats: 0,
    final_weight: false,
    name: ""
  };

  handleChange = name => event => {
    const { value } = event.target;
    const parsedNumber = value ? parseFloat(value, 10) : 0;
    this.setState({
      [name]: parsedNumber
    });
  };

  render() {
    const { weight, calories, carbs, fats, proteins, final_weight, name } = this.state;


    return (
      <div>
        <div className="form-group form-check">
          <input
            id="food-form__final"
            type="checkbox"
            className="form-check-input"
            onChange={e => this.setState({ final_weight: e.target.checked })}
            checked={final_weight}
          />
          <label className="form-check-label" htmlFor="food-form__final">
            Конечные данные (не на 100 грамм)
          </label>
        </div>
        <FormControl>
          <InputLabel htmlFor="adornment-password">Название продукта</InputLabel>
          <Input
            value={name}
            onChange={e => this.setState({ name: e.target.value })}
          />
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="adornment-password">Калории</InputLabel>
          <Input value={calories} onChange={this.handleChange("calories")} />
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="adornment-password">Белки</InputLabel>
          <Input value={proteins} onChange={this.handleChange("proteins")} />
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="adornment-password">Жиры</InputLabel>
          <Input value={fats} onChange={this.handleChange("fats")} />
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="adornment-password">Углеводы</InputLabel>
          <Input value={carbs} onChange={this.handleChange("carbs")} />
        </FormControl>
        <div>
          <button
            type="button"
            className="btn btn-primary"
            onClick={e => {
              e.preventDefault();
              this.props.submitFood(this.state);
            }}
          >
            Добавить продукт
          </button>
        </div>
      </div>
    );
  }
}


const mapDispatch = ({ food: { submitFood } }) => ({
  submitFood: food => {
    submitFood(food);
  }
});


export default connect(null, mapDispatch)(FoodForm);
