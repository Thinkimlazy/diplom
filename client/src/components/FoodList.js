import React, {Component} from "react";
import axios from "axios";
import {connect} from "react-redux";


const FoodRow = ({id, name, proteins, fats, carbs, calories}) => (
  <tr key={id}>
    <td>{name}</td>
    <td>{proteins}</td>
    <td>{fats}</td>
    <td>{carbs}</td>
    <td>{calories}</td>
    <td>
      <button
        className="btn btn-warning"
        onClick={() => this.deleteFood(id)}
      >
        Удалить
      </button>
    </td>
  </tr>
)

class FoodList extends Component {
  componentDidMount() {
    this.props.reqFoods();
  }

  deleteFood = async id => {
    await axios.delete(`/api/food?id=${id}`);
    this.props.reqFoods();
  };

  render() {
    const {foods} = this.props;

    const finalized = foods.filter(f => f.final_weight)
    const notFinalized = foods.filter(f => !f.final_weight)

    return (
      <div>
        <h3>Продукты</h3>
        <h5>Значения на 100 грамм</h5>
        <table className="table">
          <thead>
          <tr>
            <th scope="col">Название</th>
            <th scope="col">Белки</th>
            <th scope="col">Жиры</th>
            <th scope="col">Углеводы</th>
            <th scope="col">Калории</th>
          </tr>
          </thead>
          <tbody>
          {notFinalized.map(FoodRow)}

          </tbody>
        </table>

        <h5 style={{whiteSpace: 'nowrap'}}>Продукты с абсолютным значением</h5>

        <table className="table">
          <thead>
          <tr>
            <th scope="col">Название</th>
            <th scope="col">Белки</th>
            <th scope="col">Жиры</th>
            <th scope="col">Углеводы</th>
            <th scope="col">Калории</th>
          </tr>
          </thead>
          <tbody>
          {finalized.map(FoodRow)}
          </tbody>
        </table>


      </div>
    );
  }
}

const mapState = state => ({
  foods: state.food
});

const mapDispatch = ({food: {reqFoods}}) => ({
  reqFoods
});

export default connect(
  mapState,
  mapDispatch
)(FoodList);
