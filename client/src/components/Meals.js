import React, { Component } from "react";
import { connect } from "react-redux";
import moment from "moment";
import DatePicker from "react-datepicker";
import Fuse from "fuse.js";
import classNames from "classnames";
import axios from 'axios';

const dateFormat = "YYYY-MM-DD";

class Meals extends Component {
  state = {
    selectedDate: moment(),
    name: "",
    weight: 100,
    selectedFoodId: null
  };

  componentDidMount() {
    this.props.reqMeals(this.state.selectedDate.format(dateFormat));
    this.props.reqFoods();
  }

  changeDate = x => {
    this.setState({ selectedDate: x });
    this.props.reqMeals(x.format(dateFormat));
  };

  onFoodClick = selectedFoodId => this.setState({ selectedFoodId });

  submitFood = async () => {
    await axios.post('/api/meal', {
      food: this.state.selectedFoodId,
      weight: this.state.weight,
      eated_at: this.state.selectedDate.format()
    })
    this.props.reqMeals(this.state.selectedDate.format(dateFormat));
    this.setState({
      name: "",
      weight: 100,
      selectedFoodId: null
    })
  };

  changeWeight = event =>
    this.setState({ weight: parseFloat(event.target.value, 10) });

  render() {
    const { food, meals } = this.props;
    const { selectedDate, selectedFoodId } = this.state;

    const options = {
      shouldSort: true,
      threshold: 0.5,
      location: 0,
      distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys: ["name"]
    };

    const fuse = new Fuse(food, options);
    let result = fuse.search(this.state.name);
    result = result.length ? result : food;

    const selectedFood = food.find(x => x.id === selectedFoodId);

    const dayRes = meals.reduce(
      (acc, meal) => {
        const mealFood = food.find(x => x.id === meal.food);
        if (!mealFood) return acc;
        const k = mealFood.final_weight ? 1 : meal.weight / 100;
        return {
          carbs: acc.carbs + mealFood.carbs * k,
          proteins: acc.proteins + mealFood.proteins * k,
          fats: acc.fats + mealFood.fats * k,
          calories: acc.calories + mealFood.calories * k
        };
      },
      { carbs: 0, proteins: 0, fats: 0, calories: 0 }
    );

    return (
      <div className="container" style={{ marginTop: 32 }}>
        <div className="row">
          <div className="col-sm">
            {selectedFoodId ? (
              <div>
                {selectedFood.name}
                <br />
                <button
                  type="button"
                  className="btn btn-outline-secondary"
                  onClick={e => this.setState({ selectedFoodId: null })}
                >
                  Перевыбрать
                </button>
                <br />
                {selectedFood["final_weight"] || (
                  <div className="form-group">
                    <label htmlFor="weight">Вес</label>
                    <input
                      onChange={this.changeWeight}
                      value={this.state.weight}
                      type="number"
                      className="form-control"
                      id="weight"
                      placeholder="Вес"
                    />
                  </div>
                )}

                <button className="btn btn-primary" onClick={this.submitFood}>
                  Добавить
                </button>
              </div>
            ) : (
              <React.Fragment>
                <div>
                  <div className="form-group">
                    <label htmlFor="exampleInputPassword1">
                      Название продукты
                    </label>
                    <input
                      value={this.state.name}
                      onChange={e => this.setState({ name: e.target.value })}
                      type="text"
                      className="form-control"
                      id="exampleInputPassword1"
                      placeholder="Название продукта"
                    />
                  </div>
                </div>
                <table className="table table-hover">
                  <thead>
                    <tr>
                      <th scope="col">Название</th>
                      <th scope="col">Белки</th>
                      <th scope="col">Жиры</th>
                      <th scope="col">Углеводы</th>
                      <th scope="col">Калории</th>
                    </tr>
                  </thead>
                  <tbody>
                    {result.map(food => (
                      <tr
                        key={food.id}
                        style={{ cursor: "pointer" }}
                        onClick={() => this.onFoodClick(food.id)}
                      >
                        <td>{food.name}</td>
                        <td>{food.proteins}</td>
                        <td>{food.fats}</td>
                        <td>{food.carbs}</td>
                        <td>{food.calories}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </React.Fragment>
            )}
          </div>
          <div className="col-sm">
            <DatePicker
              locale="ru"
              selected={selectedDate}
              onChange={this.changeDate}
            />
            <h3>Значения в итог</h3>
            <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Название</th>
                  <th scope="col">Белки</th>
                  <th scope="col">Жиры</th>
                  <th scope="col">Углеводы</th>
                  <th scope="col">Калории</th>
                  <th scope="col">Вес</th>
                </tr>
              </thead>
              <tbody>
                {meals.map(meal => {
                  const mealFood = food.find(x => x.id === meal.food);
                  if (!mealFood) return null;
                  const k = mealFood.final_weight ? 1 : meal.weight / 100;

                  return (
                    <tr key={meal.id}>
                      <td>{mealFood.name}</td>
                      <td>{mealFood.proteins * k}</td>
                      <td>{mealFood.fats * k}</td>
                      <td>{mealFood.carbs * k}</td>
                      <td>{mealFood.calories * k}</td>
                      <td>{mealFood.final_weight || meal.weight}</td>
                    </tr>
                  );
                })}
                <tr>
                  <td>
                    <b>Итог</b>
                  </td>
                  <td>{dayRes.proteins.toFixed(2)}</td>
                  <td>{dayRes.fats.toFixed(2)}</td>
                  <td>{dayRes.carbs.toFixed(2)}</td>
                  <td>{dayRes.calories.toFixed(2)}</td>
                  <td />
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

const mapState = state => ({
  meals: state.meals,
  food: state.food
});

const mapDispatch = ({ meals: { reqMeals }, food: { reqFoods } }) => ({
  reqMeals,
  reqFoods
});

export default connect(
  mapState,
  mapDispatch
)(Meals);
