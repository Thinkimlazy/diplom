import React, { Component } from "react";
import axios from "axios";
import { connect } from "react-redux";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";


import 'react-datepicker/dist/react-datepicker-cssmodules.css';

import FoodList from "./components/FoodList";
import FoodForm from "./components/FoodForm";
import Meals from "./components/Meals";


const Products = () => (
  <div className="container">
  <div className="row">
    <div className="col-sm">
      <FoodList />
    </div>
    <div className="col-sm">
      <FoodForm />
    </div>
  </div>
</div>
)

class App extends Component {
  async componentDidMount() {
    const res = await axios("/api/food");
  }

  handleChange = name => event => {
    const { value } = event.target;
    const parsedNumber = value ? parseFloat(value, 10) : 0;
    this.setState({
      [name]: parsedNumber
    });
  };

  render() {
    return (
      <Router>
        <div>
          <div className="nav">
            <Link to="/products" className="navbar__item">
              Продукты
            </Link>
            <Link to="/meals" className="navbar__item">
              Питание
            </Link>
          </div>
          <Route path="/products" component={Products} />
          <Route path="/meals" component={Meals} />
        </div>
      </Router>
    );
  }
}

export default App;
