import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.css'
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import * as models from './models'
import { init } from '@rematch/core'
import { Provider } from 'react-redux'

import moment from 'moment';
import 'moment/locale/ru';

const store = init({
    models
})

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();
